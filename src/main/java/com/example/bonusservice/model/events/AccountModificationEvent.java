package com.example.bonusservice.model.events;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

public class AccountModificationEvent {

    private UUID transactionId;
    private BigDecimal value;
    private BigDecimal accountedValue;

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getAccountedValue() {
        return accountedValue;
    }

    public void setAccountedValue(BigDecimal accountedValue) {
        this.accountedValue = accountedValue;
    }

    public UUID getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(UUID transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountModificationEvent that = (AccountModificationEvent) o;
        return Objects.equals(transactionId, that.transactionId) && Objects.equals(value, that.value) && Objects.equals(accountedValue, that.accountedValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionId, value, accountedValue);
    }
}
