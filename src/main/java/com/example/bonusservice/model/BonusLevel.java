package com.example.bonusservice.model;

public enum BonusLevel {
    Basic, Bronze, Silver, Gold
}
