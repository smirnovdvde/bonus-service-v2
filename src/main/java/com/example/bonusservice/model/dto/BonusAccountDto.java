package com.example.bonusservice.model.dto;

import com.example.bonusservice.model.BonusLevel;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

public class BonusAccountDto {

    private BigDecimal value;
    private BonusLevel level;
    private ZonedDateTime lastUpdate;

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BonusLevel getLevel() {
        return level;
    }

    public void setLevel(BonusLevel level) {
        this.level = level;
    }

    public ZonedDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(ZonedDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BonusAccountDto that = (BonusAccountDto) o;
        return Objects.equals(value, that.value) && level == that.level && Objects.equals(lastUpdate, that.lastUpdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, level, lastUpdate);
    }
}
