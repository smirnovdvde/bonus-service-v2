package com.example.bonusservice.model.entities;

import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

public class TransactionEntity {


    public enum State {
        REJECTED, COMMITTED
    }

    private UUID accountId;

    private State state;

    private ZonedDateTime timestamp;

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionEntity that = (TransactionEntity) o;
        return Objects.equals(accountId, that.accountId) && state == that.state && Objects.equals(timestamp, that.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, state, timestamp);
    }
}
