package com.example.bonusservice.model.entities;

import com.example.bonusservice.model.BonusLevel;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

public class AccountEntity {
    private BigDecimal value;
    private BigDecimal accountedAmount;
    private BonusLevel level;
    private ZonedDateTime lastUpdateTimestamp;

    private UUID lastTransactionId;

    private boolean lastTransactionAccepted;

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getAccountedAmount() {
        return accountedAmount;
    }

    public void setAccountedAmount(BigDecimal accountedAmount) {
        this.accountedAmount = accountedAmount;
    }

    public BonusLevel getLevel() {
        return level;
    }

    public void setLevel(BonusLevel level) {
        this.level = level;
    }

    public ZonedDateTime getLastUpdateTimestamp() {
        return lastUpdateTimestamp;
    }

    public void setLastUpdateTimestamp(ZonedDateTime lastUpdateTimestamp) {
        this.lastUpdateTimestamp = lastUpdateTimestamp;
    }

    public UUID getLastTransactionId() {
        return lastTransactionId;
    }

    public void setLastTransactionId(UUID lastTransactionId) {
        this.lastTransactionId = lastTransactionId;
    }

    public boolean isLastTransactionAccepted() {
        return lastTransactionAccepted;
    }

    public void setLastTransactionAccepted(boolean lastTransactionAccepted) {
        this.lastTransactionAccepted = lastTransactionAccepted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountEntity that = (AccountEntity) o;
        return lastTransactionAccepted == that.lastTransactionAccepted && Objects.equals(value, that.value) && Objects.equals(accountedAmount, that.accountedAmount) && level == that.level && Objects.equals(lastUpdateTimestamp, that.lastUpdateTimestamp) && Objects.equals(lastTransactionId, that.lastTransactionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, accountedAmount, level, lastUpdateTimestamp, lastTransactionId, lastTransactionAccepted);
    }
}
