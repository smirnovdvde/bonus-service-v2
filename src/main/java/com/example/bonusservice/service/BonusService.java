package com.example.bonusservice.service;

import com.example.bonusservice.model.BonusLevel;
import com.example.bonusservice.model.entities.AccountEntity;
import com.example.bonusservice.model.entities.TransactionEntity;
import com.example.bonusservice.model.events.AccountModificationEvent;
import com.example.transactionservice.model.events.TransactionEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.config.TopicConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.UUIDSerializer;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
public class BonusService {

    private static final String ACCOUNT_STORE_NAME = "bonus_account";
    private static final String TRANSACTION_STORE_NAME = "bonus_transaction";

    private static final String ACCOUNT_EVENT_TOPIC_NAME = "bonus_service-bonus_account-events";
    private static final String TRANSACTION_STORE_TOPIC_NAME = "bonus_service-bonus-transaction-store";

    private static final Logger log = LoggerFactory.getLogger(BonusService.class);

    private static final UUIDSerializer uuidSerializer = new UUIDSerializer();
    @Value("${transaction.eventTopicName}")
    private String transactionEventTopicName;

    @Value("${streams.changelogMinIsr:1}")
    private int changelogMinIsr;

    private final ObjectMapper objectMapper;
    private final StreamsBuilderFactoryBean streamsBuilderFactoryBean;
    private final KafkaTemplate<UUID, Object> kafkaTemplate;

    private final RestTemplate restTemplate;

    @Autowired
    public BonusService(ObjectMapper objectMapper, StreamsBuilderFactoryBean streamsBuilderFactoryBean, KafkaTemplate<UUID, Object> kafkaTemplate, RestTemplate restTemplate) {
        this.objectMapper = objectMapper;
        this.streamsBuilderFactoryBean = streamsBuilderFactoryBean;
        this.kafkaTemplate = kafkaTemplate;
        this.restTemplate = restTemplate;
    }

    @PostConstruct
    public void buildTransactionEventPipeline() throws Exception {


        final var streamsBuilder = Objects.requireNonNull(streamsBuilderFactoryBean.getObject());

        streamsBuilder
                .stream(
                        transactionEventTopicName,
                        Consumed.with(
                                Serdes.UUID(),
                                jsonSerde(TransactionEvent.class)
                        )
                )
                .filter((id, event) -> event.amount.compareTo(BigDecimal.ZERO) < 0)
                .mapValues((id, event) -> {
                    final var modificationEvent = new AccountModificationEvent();
                    modificationEvent.setTransactionId(UUID.randomUUID());
                    modificationEvent.setAccountedValue(event.reverted ? event.amount : event.amount.negate());
                    modificationEvent.setValue(defineBonusAmount(modificationEvent.getAccountedValue()));
                    return modificationEvent;
                })
                .to(
                        ACCOUNT_EVENT_TOPIC_NAME,
                        Produced.with(
                                Serdes.UUID(),
                                jsonSerde(AccountModificationEvent.class)
                        )
                );

        streamsBuilder
                .stream(
                        ACCOUNT_EVENT_TOPIC_NAME,
                        Consumed.with(
                                Serdes.UUID(),
                                jsonSerde(AccountModificationEvent.class)
                        )
                )
                .groupByKey()
                .aggregate(
                        () -> {
                            final var account = new AccountEntity();
                            account.setValue(BigDecimal.ZERO);
                            account.setAccountedAmount(BigDecimal.ZERO);
                            account.setLevel(defineLevel(account.getAccountedAmount()));
                            account.setLastUpdateTimestamp(ZonedDateTime.now());
                            return account;
                        },
                        (id, event, account) -> {
                            if (account.getValue().add(event.getValue()).compareTo(BigDecimal.ZERO) >= 0) {
                                account.setValue(account.getValue().add(event.getValue()));
                                account.setAccountedAmount(account.getAccountedAmount().add(event.getAccountedValue()));
                                account.setLevel(defineLevel(account.getValue()));
                                account.setLastUpdateTimestamp(ZonedDateTime.now());
                                account.setLastTransactionAccepted(true);

                                log.debug("account {} accept tx {}: {}", id, event.getTransactionId(), event);
                            } else {
                                account.setLastTransactionAccepted(false);
                                log.debug("account {} reject tx {}: {}", id, event.getTransactionId(), event);
                            }
                            account.setLastTransactionId(event.getTransactionId());
                            account.setLastUpdateTimestamp(ZonedDateTime.now());
                            return account;
                        },
                        Materialized
                                .<UUID, AccountEntity, KeyValueStore<Bytes, byte[]>>as(ACCOUNT_STORE_NAME)
                                .withKeySerde(Serdes.UUID())
                                .withValueSerde(jsonSerde(AccountEntity.class))
                                .withLoggingEnabled(
                                        Map.of(
                                                TopicConfig.MIN_IN_SYNC_REPLICAS_CONFIG, Integer.toString(changelogMinIsr)
                                        )
                                )
                )
                .toStream()
                .map((accountId, account) -> {
                    final var transaction = new TransactionEntity();
                    transaction.setAccountId(accountId);
                    transaction.setState(account.isLastTransactionAccepted() ? TransactionEntity.State.COMMITTED : TransactionEntity.State.REJECTED);
                    transaction.setTimestamp(account.getLastUpdateTimestamp());
                    return KeyValue.pair(account.getLastTransactionId(), transaction);
                })
                .to(
                        TRANSACTION_STORE_TOPIC_NAME,
                        Produced.with(
                                Serdes.UUID(),
                                jsonSerde(TransactionEntity.class)
                        )
                );

        final var transactionTable = streamsBuilder
                .globalTable(
                        TRANSACTION_STORE_TOPIC_NAME,
                        Consumed.with(
                                Serdes.UUID(),
                                jsonSerde(TransactionEntity.class)
                        ),
                        Materialized.as(TRANSACTION_STORE_NAME)
                );
    }

    public Optional<AccountEntity> findAccount(UUID id) {
        return rpcGet(ACCOUNT_STORE_NAME, id, AccountEntity.class);
    }

    @Transactional
    public void startWithdrawalTransaction(UUID id, UUID accountId, BigDecimal amount) {
        if (amount.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("invalid amount");
        }
        final var event = new AccountModificationEvent();
        event.setValue(amount.negate());
        event.setAccountedValue(BigDecimal.ZERO);
        event.setTransactionId(id);

        kafkaTemplate.send(ACCOUNT_EVENT_TOPIC_NAME, accountId, event);
    }

    public Optional<TransactionEntity> findTransaction(UUID id) {
        return Optional.ofNullable(
                Objects.requireNonNull(streamsBuilderFactoryBean.getKafkaStreams(), "kafka streams not ready").store(
                        StoreQueryParameters.fromNameAndType(TRANSACTION_STORE_NAME, QueryableStoreTypes.<UUID, TransactionEntity>keyValueStore())
                ).get(id)
        );
    }

    private <T> JsonSerde<T> jsonSerde(Class<T> tClass) {
        return new JsonSerde<>(tClass, objectMapper);
    }

    private <T> Optional<T> rpcGet(String store, UUID id, Class<T> tClass) {
        final var streams = Objects.requireNonNull(streamsBuilderFactoryBean.getKafkaStreams(), "kafka streams not ready");
        final var metadata = streams.queryMetadataForKey(ACCOUNT_STORE_NAME, id, uuidSerializer);
        log.debug("request {} from store {} by id {} via RPC {}", tClass, store, id, metadata.activeHost());
        try {
            return Optional.ofNullable(
                    restTemplate
                            .getForEntity(
                                    "http://" + metadata.activeHost().host() + ":" + metadata.activeHost().port() + "/streamsrpc/get/" + store + "/" + id,
                                    tClass
                            ).getBody()
            );
        } catch (HttpClientErrorException.NotFound e) {
            return Optional.empty();
        }
    }

    private static BonusLevel defineLevel(BigDecimal value) {
        if (BigDecimal.valueOf(1_000).compareTo(value) > 0) {
            return BonusLevel.Basic;
        } else if (BigDecimal.valueOf(10_000).compareTo(value) > 0) {
            return BonusLevel.Bronze;
        } else if (BigDecimal.valueOf(100_000).compareTo(value) > 0) {
            return BonusLevel.Silver;
        } else {
            return BonusLevel.Gold;
        }
    }

    private static BigDecimal defineBonusAmount(BigDecimal value) {
        return value.multiply(BigDecimal.valueOf(1, 2));
    }
}
