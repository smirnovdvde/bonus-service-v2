package com.example.bonusservice.controller;

import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.config.StreamsBuilderFactoryBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;
import java.util.UUID;

@RestController
public class StreamsRPCController {

    private static final Logger log = LoggerFactory.getLogger(StreamsRPCController.class);

    private final StreamsBuilderFactoryBean streamsBuilderFactoryBean;

    @Autowired
    public StreamsRPCController(StreamsBuilderFactoryBean streamsBuilderFactoryBean) {
        this.streamsBuilderFactoryBean = streamsBuilderFactoryBean;
    }

    @GetMapping("streamsrpc/get/{store}/{id}")
    public ResponseEntity<Object> getObject(@PathVariable("store") String store, @PathVariable("id") UUID id) {
        log.debug("get store={} id={}", store, id);
        final var object = Objects.requireNonNull(streamsBuilderFactoryBean.getKafkaStreams(), "kafka streams not ready").store(
                StoreQueryParameters.fromNameAndType(store, QueryableStoreTypes.keyValueStore())
        ).get(id);
        if (object != null) {
            log.debug("get store={} id={} : found", store, id);
            return ResponseEntity.ok().body(object);
        } else {
            log.warn("get store={} id={}: not found", store, id);
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("streamsrpc/topology")
    public String getTopology() {
        return Objects.requireNonNull(Objects.requireNonNull(streamsBuilderFactoryBean.getTopology()).describe().toString());
    }
}
