package com.example.bonusservice.controller;

import com.example.bonusservice.model.dto.BonusTransactionCreateDto;
import com.example.bonusservice.model.dto.BonusTransactionStatusDto;
import com.example.bonusservice.model.entities.TransactionEntity;
import com.example.bonusservice.service.BonusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

@RestController
public class BonusTransactionController {

    private static final Logger log = LoggerFactory.getLogger(BonusTransactionController.class);

    private final BonusService bonusService;

    @Autowired
    public BonusTransactionController(BonusService bonusService) {
        this.bonusService = bonusService;
    }

    @PutMapping(path = "transactions/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> executeTransaction(@PathVariable("id") UUID id, @RequestBody BonusTransactionCreateDto transactionDto) {
        log.debug("put transaction {}", id);
        bonusService.startWithdrawalTransaction(
                id,
                transactionDto.getFrom(),
                transactionDto.getAmount()
        );
        return ResponseEntity.noContent().build();
    }

    @GetMapping(path = "transactions/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public BonusTransactionStatusDto getTransaction(@PathVariable("id") UUID id) {
        log.debug("get transaction {}", id);
        return bonusService.findTransaction(id).map(entity -> {
            final var transactionDto = new BonusTransactionStatusDto();
            transactionDto.setFrom(entity.getAccountId());

            transactionDto.setExecutionTimestamp(entity.getTimestamp());
            transactionDto.setAccepted(entity.getState() == TransactionEntity.State.COMMITTED);
            return transactionDto;
        }).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
}
